import React, { Component } from "react";

class ProjectFeature extends Component {
    state = {};
    render() {
        return (
            <div className="project-feature">
                <h1>Project Feature</h1>
                <ul>
                    <li>Responsive Layout</li>
                    <li>Font Awesome Icons</li>
                    <li>Clean &#38; Commented Code</li>
                    <li>Pixel perfect Design</li>
                    <li>Highly Customizable</li>
                </ul>
            </div>
        );
    }
}

export default ProjectFeature;
