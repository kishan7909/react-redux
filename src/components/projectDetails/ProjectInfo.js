/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";

class ProjectInfo extends Component {
    state = {};
    render() {
        return (
            <div className="post-info">
                <h1>Project Info</h1>
                <ul>
                    <li>
                        <span>
                            <i className="fa fa-user" />
                        </span>
                        <a href="#">Premium Layers</a>
                    </li>
                    <li>
                        <span>
                            <i className="fa fa-heart" />
                        </span>
                        <a href="#">138 Likes</a>
                    </li>
                    <li>
                        <span>
                            <i className="fa fa-calendar" />
                        </span>
                        <a href="#">14 Jannuary, 2014</a>
                    </li>
                    <li>
                        <span>
                            <i className="fa fa-link" />
                        </span>
                        <a href="#">http:www.themeforest.net</a>
                    </li>
                </ul>
            </div>
        );
    }
}

export default ProjectInfo;
