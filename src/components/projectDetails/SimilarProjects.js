import React, { Component } from "react";
import { NavLink as Link } from "react-router-dom";
class SimilarProjects extends Component {
    state = {};
    render() {
        return (
            <div className="similar-projects">
                <h1>Similar Projects</h1>
                <div className="portfolio-box">
                    {this.props.similarProjects.map((project, index) => {
                        return (
                            <div
                                className="project-post"
                                style={{ marginBottom: "20px" }}
                                key={index}
                            >
                                <img alt="" src={project.image} />
                                <div className="hover-box">
                                    <div className="project-title">
                                        <h2>{project.title}</h2>
                                        <span>{project.title2}</span>
                                        <div>
                                            <Link to={"/project/" + project.id}>
                                                <i className="fa fa-arrow-right" />
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        );
                    })}
                </div>
            </div>
        );
    }
}

export default SimilarProjects;
