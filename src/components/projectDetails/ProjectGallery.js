/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";

class ProjectGallery extends Component {
    state = {};
    render() {
        return (
            <div className="project-gallery">
                <h1>Project Gallery</h1>
                <ul>
                    <li>
                        <a href="#">
                            <img alt="" src="../../assert/upload/thumb1.jpg" />
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img alt="" src="../../assert/upload/thumb2.jpg" />
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img alt="" src="../../assert/upload/thumb3.jpg" />
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img alt="" src="../../assert/upload/thumb4.jpg" />
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img alt="" src="../../assert/upload/thumb5.jpg" />
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img alt="" src="../../assert/upload/thumb6.jpg" />
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img alt="" src="../../assert/upload/thumb7.jpg" />
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img alt="" src="../../assert/upload/thumb8.jpg" />
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img alt="" src="../../assert/upload/thumb9.jpg" />
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img alt="" src="../../assert/upload/thumb10.jpg" />
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img alt="" src="../../assert/upload/thumb11.jpg" />
                        </a>
                    </li>
                </ul>
            </div>
        );
    }
}

export default ProjectGallery;
