/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { IsotopFilter } from "../projects/Isotope";

class SidebarFilter extends Component {
    state = {};
    render() {
        return (
            <div className="filter-box">
                <h3>
                    Filter<i className="fa fa-th-large" />
                </h3>
                <ul className="filter">
                    <li>
                        <span
                            className="active"
                            onClick={() => IsotopFilter("portfolio-box", "*")}
                        >
                            All Works
                        </span>
                    </li>
                    <li>
                        <span
                            onClick={() =>
                                IsotopFilter("portfolio-box", ".Webdesign")
                            }
                        >
                            Web Design
                        </span>
                    </li>
                    <li>
                        <span
                            onClick={() =>
                                IsotopFilter("portfolio-box", ".Photography")
                            }
                        >
                            Photography
                        </span>
                    </li>
                    <li>
                        <span
                            onClick={() =>
                                IsotopFilter("portfolio-box", ".Illustration")
                            }
                        >
                            Illustration
                        </span>
                    </li>
                    <li>
                        <span
                            onClick={() =>
                                IsotopFilter("portfolio-box", ".Nature")
                            }
                        >
                            Nature
                        </span>
                    </li>
                    <li>
                        <span
                            onClick={() =>
                                IsotopFilter("portfolio-box", ".Logo")
                            }
                        >
                            Logo
                        </span>
                    </li>
                </ul>
            </div>
        );
    }
}

export default SidebarFilter;
