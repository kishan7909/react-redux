import React, { Component } from "react";
import ProjectSlider from "../projectDetails/ProjectSlider";
import ProjectText from "../projectDetails/ProjectText";
import SimilarProjects from "../projectDetails/SimilarProjects";
import ProjectInfo from "../projectDetails/ProjectInfo";
import ProjectGallery from "../projectDetails/ProjectGallery";
import ProjectFeature from "../projectDetails/ProjectFeature";
import { connect } from "react-redux";
import Sidebar from "../Navigation/Sidebar";

class ProjectDetails extends Component {
    state = {};
    render() {
        const path = this.props.match.path;
        if (path === "/project/:id") {
            localStorage.setItem("active", "active");
        }

        const id = this.props.match.params.id;
        const project = this.props.project.filter(project => {
            return project.id === id;
        });
        const similarproject = this.props.project.filter(smproject => {
            return smproject.type === project[0].type;
        });
        console.log(project);
        console.log("Similar Project", similarproject);
        return (
            <React.Fragment>
                <Sidebar filter={false} />
                <div id="content">
                    <div className="inner-content">
                        <div className="single-project">
                            <div className="single-box">
                                <div className="single-box-content">
                                    <div className="project-post-content">
                                        <ProjectSlider />
                                        <ProjectText
                                            text={
                                                project[0].details.projectText
                                            }
                                            title={
                                                project[0].details
                                                    .projectTextTitle
                                            }
                                        />
                                        <SimilarProjects
                                            similarProjects={similarproject}
                                        />
                                    </div>
                                </div>
                                <div className="sidebar">
                                    <ProjectInfo />
                                    <ProjectGallery />
                                    <ProjectFeature />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
const mapStateToProps = state => {
    return {
        project: state.projects.project
    };
};

export default connect(mapStateToProps)(ProjectDetails);
