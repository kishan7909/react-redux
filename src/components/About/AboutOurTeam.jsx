import React from "react";

const AboutOurTeam = () => {
    return (
        <div className="about-section last-section">
            <h1>Meet The Team</h1>
            <div className="team-members">
                <div className="row">
                    <div className="col-md-4">
                        <div className="team-post">
                            <img alt="" src="../../assert/upload/team1.jpg" />
                            <div className="team-hover">
                                <div className="team-data">
                                    <h3>John Smith</h3>
                                    <span>Web Developer</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="team-post">
                            <img alt="" src="../../assert/upload/team2.jpg" />
                            <div className="team-hover">
                                <div className="team-data">
                                    <h3>John Smith</h3>
                                    <span>Web Developer</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="team-post">
                            <img alt="" src="../../assert/upload/team3.jpg" />
                            <div className="team-hover">
                                <div className="team-data">
                                    <h3>John Smith</h3>
                                    <span>Web Developer</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-4">
                        <div className="team-post">
                            <img alt="" src="../../assert/upload/team4.jpg" />
                            <div className="team-hover">
                                <div className="team-data">
                                    <h3>John Smith</h3>
                                    <span>Web Developer</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="team-post">
                            <img alt="" src="../../assert/upload/team5.jpg" />
                            <div className="team-hover">
                                <div className="team-data">
                                    <h3>John Smith</h3>
                                    <span>Web Developer</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="team-post">
                            <img alt="" src="../../assert/upload/team1.jpg" />
                            <div className="team-hover">
                                <div className="team-data">
                                    <h3>John Smith</h3>
                                    <span>Web Developer</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default AboutOurTeam;
