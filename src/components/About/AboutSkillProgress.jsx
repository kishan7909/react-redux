import React from "react";

const AboutSkillProgress = () => {
    return (
        <div className="skills-progress">
            <h1>Our Skills</h1>
            <p>
                Frontend Development <span>71%</span>
            </p>
            <div className="meter nostrips frontend">
                <span style={{ width: "7%" }} />
            </div>
            <p>
                Photoshop <span>85%</span>
            </p>
            <div className="meter nostrips photoshop">
                <span style={{ width: "85%" }} />
            </div>
            <p>
                Wordpress <span>76%</span>
            </p>
            <div className="meter nostrips wp">
                <span style={{ width: "76%" }} />
            </div>
            <p>
                Plugins <span>53%</span>
            </p>
            <div className="meter nostrips plugins">
                <span style={{ width: "53%" }} />
            </div>
        </div>
    );
};

export default AboutSkillProgress;
