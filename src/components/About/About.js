import React, { Component } from "react";
import AboutUs from "./AboutUs";
import AboutOurTeam from "./AboutOurTeam";
import AboutSkillProgress from "./AboutSkillProgress";
import AboutClientTestimonials from "./AboutClientTestimonials";
import Sidebar from "../Navigation/Sidebar";

class About extends Component {
    state = {};

    render() {
        return (
            <React.Fragment>
                <Sidebar filter={false} />
                <div id="content">
                    <div className="inner-content">
                        <div className="about-page">
                            <div className="about-box">
                                <div className="about-content">
                                    <AboutUs />
                                    <AboutOurTeam />
                                </div>
                                <div className="sidebar">
                                    <AboutSkillProgress />
                                    <AboutClientTestimonials />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default About;
