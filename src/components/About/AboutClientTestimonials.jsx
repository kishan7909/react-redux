import React from "react";

const AboutClientTestimonials = () => {
    return (
        <div className="testimonial">
            <h1>Client Testimonials</h1>
            <div className="quovolve-box play">
                <ul
                    className="quovolve"
                    style={{ position: "relative", height: "auto" }}
                >
                    <li>
                        <div className="client-test">
                            <img alt="" src="../../assert/upload/avatar2.jpg" />
                            <h3>Collis Ta’eed - CEO at Envato</h3>
                        </div>
                        <p>
                            Sollicitudin lorem quis bibendum auctor, nisi elit
                            consequat ipsum, nec sagittis sem nibh id elit. Duis
                            sed odio sit amet nibh vulputate cursus a sit amet
                            mauris. Morbi accumsan ipsum velit. Nam nec tellus a
                            odio tincidunt.
                        </p>
                    </li>
                    <li>
                        <div className="client-test">
                            <img alt="" src="../../assert/upload/avatar1.jpg" />
                            <h3>John Smith - Web Developer</h3>
                        </div>
                        <p>
                            Sollicitudin lorem quis bibendum auctor, nisi elit
                            consequat ipsum, nec sagittis sem nibh id elit. Duis
                            sed odio sit amet nibh vulputate cursus a sit amet
                            mauris. Morbi accumsan ipsum velit. Nam nec tellus a
                            odio tincidunt.
                        </p>
                    </li>
                </ul>
            </div>
        </div>
    );
};

export default AboutClientTestimonials;
