import React, { Component } from "react";

import { BrowserRouter as Router, Route } from "react-router-dom";
import Project from "./components/projects/Project";
// import Sidebar from "./components/Navigation/Sidebar";
import ProjectDetails from "./components/projects/ProjectDetails";
import About from "./components/About/About";

class App extends Component {
    render() {
        return (
            <Router>
                <div id="container">
                    {/* <Sidebar filter={true} /> */}
                    <Route exact path="/" component={Project} />
                    <Route path="/about" component={About} />
                    <Route path="/project/:id" component={ProjectDetails} />
                </div>
            </Router>
        );
    }
}

export default App;
