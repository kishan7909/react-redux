import img1 from "../../assert/upload/image1.jpg";
import img2 from "../../assert/upload/image2.jpg";
import img3 from "../../assert/upload/image3.jpg";
import img4 from "../../assert/upload/image4.jpg";
import img5 from "../../assert/upload/image5.jpg";
import img6 from "../../assert/upload/image6.jpg";
import img7 from "../../assert/upload/image7.jpg";
import img8 from "../../assert/upload/image8.jpg";
import img9 from "../../assert/upload/image9.jpg";
import img10 from "../../assert/upload/image10.jpg";
import img11 from "../../assert/upload/image11.jpg";
import img12 from "../../assert/upload/image12.jpg";

const initstate = {
    project: [
        {
            id: "1",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img1,
            type: "Webdesign",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                }
            }
        },
        {
            id: "2",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img2,
            type: "Webdesign",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                }
            }
        },
        {
            id: "3",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img3,
            type: "Photography",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                }
            }
        },
        {
            id: "4",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img4,
            type: "Photography",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                }
            }
        },
        {
            id: "5",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img5,
            type: "Illustration",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                }
            }
        },
        {
            id: "6",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img6,
            type: "Illustration",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                }
            }
        },
        {
            id: "7",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img7,
            type: "Illustration",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                }
            }
        },
        {
            id: "8",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img8,
            type: "Nature",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                }
            }
        },
        {
            id: "9",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img9,
            type: "Nature",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                }
            }
        },
        {
            id: "10",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img10,
            type: "Logo",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                }
            }
        },
        {
            id: "11",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img11,
            type: "Logo",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                }
            }
        },
        {
            id: "12",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img12,
            type: "Webdesign",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                }
            }
        },
        {
            id: "13",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img1,
            type: "Illustration",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                }
            }
        },
        {
            id: "14",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img4,
            type: "Illustration",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                }
            }
        },
        {
            id: "15",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img5,
            type: "Logo",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                }
            }
        },
        {
            id: "16",
            title: "Cool App Design",
            title2: "development, mobile",
            image: img2,
            type: "Nature",
            details: {
                projectTextTitle: "Amazing Car Wallpaper",
                projectText:
                    "Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora This is Photoshop s version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet.",
                projectInfo: {
                    projectName: "Premium Layers",
                    likes: 138,
                    dates: "14 January, 2014",
                    website: "http:www.themeforest.net"
                }
            }
        }
    ]
};

const projectReducer = (state = initstate, action) => {
    console.log("projectReducer call", action);
    return state;
};

export default projectReducer;
