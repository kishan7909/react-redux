import sidebarReducer from "./sidebarReducer";
import projectReducer from "./projectReducer";
import { combineReducers } from "redux";

console.log("rootReducer call");
const rootReducer = combineReducers({
    sidebar: sidebarReducer,
    projects: projectReducer
});

export default rootReducer;
